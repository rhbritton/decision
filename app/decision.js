var React = require('react/addons')
  , PureRenderMixin = React.addons.PureRenderMixin

module.exports = React.createClass({
	  displayName:'Decision'
	, mixins: [PureRenderMixin]
	, getInitialState() {
		return {}
	},
	wheel(props, remaining=20) {
		var chosen = remaining ? props.choices[remaining%props.choices.val().size] : this.props.chosen
		
		this.setState({ chosen })
		
		if(remaining) setTimeout(function(){
			this.wheel(props, --remaining)
		}.bind(this), 200/Math.sqrt(remaining))
	},
	componentDidMount() {
		this.wheel(this.props)
	},
	componentWillReceiveProps(props) {
		this.wheel(props)
	},
	render() {
		var text = this.state.chosen && this.state.chosen.text.val()
		  , dim = Math.min(window.innerWidth, window.innerHeight)
		  , style = text && { fontSize:(dim/50/Math.pow(text.length, 0.3))+'em' }
		return <h1 className="chosen" style={style}>{text}</h1>
	}
})