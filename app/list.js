var React = require('react/addons')
  , Add = require('./add.js')
  , ListItem = require('./list-item.js')
  , Decide = require('./decide.js')
  , TransitionGroup = React.addons.CSSTransitionGroup
  , PureRenderMixin = React.addons.PureRenderMixin

module.exports = React.createClass({
	  displayName:'List'
	, mixins: [PureRenderMixin]
	, addChoice(choice) {
		if(!this.props.list.choices.val().some(function(c) {
			if(c.get('text') == choice.text) return true
		})) {
			this.props.list.choices.unshift(choice)
			return true
		} else {
			alert('That choice has already been added.')
		}
	}
	, updateTitle(e) {
		if(e.target.value.length >= 256)
			return alert('That title is too long.')
		
		this.props.list.title.set(e.target.value)
	}
	, highlightUntitled(e) {
		if(e.target.value == 'Untitled')
			this.getDOMNode().querySelector('input.name').select()
	}
	, render() {
		var self = this
		  , list = this.props.list
		  , choices = list.choices.map(function(choice, i) {
		  		return <ListItem choice={choice} index={i} key={choice.id.val()} onDelete={self.deleteChoice} onChange={self.editChoice} />
		    })
		  , empty = !choices.length && <div className="empty">Add some choices so I can choose one for you</div>
		  , decide = !!choices.length && <Decide choices={list.choices} />

		return <div>
			<div className="top">
				<div className="title section">
					<button className="manage" onClick={this.props.onBack}>&#9776;</button>
					<input type="text" className="name" value={this.props.list.title.val()} onChange={this.updateTitle} onFocus={this.highlightUntitled} />
					<button className="delete" onClick={this.props.onDelete}>&times;</button>
				</div>
				<Add onAdd={this.addChoice} />
			</div>
			<div className="choices section">
				<TransitionGroup transitionName="choice">
					{choices}
				</TransitionGroup>
				{empty}
			</div>
			{decide}
		</div>
	}
})