var React = require('react/addons')
  , PureRenderMixin = React.addons.PureRenderMixin

module.exports = React.createClass({
	  displayName:'AdMob'
	, mixins: [PureRenderMixin]
	, componentDidMount: function() {
		var admobid = {
            banner: 'ca-app-pub-7800592282242539/6252588106', // or DFP format "/6253334/dfp_example_ad"
            interstitial: 'ca-app-pub-7800592282242539/yyy'
        }

        if(typeof AdMob !== 'undefined') {
        	AdMob.createBanner({
			      adId: admobid.banner
			    , position: AdMob.AD_POSITION.TOP_CENTER
			    , autoShow: true
			    , overlap: true
			})
        }
	}
	, componentWillUnmount: function() {
		if(typeof AdMob !== 'undefined') AdMob.removeBanner()
	}
	, render: function() {
		return <div></div>
	}
})