var React = require('react/addons')
  , List = require('./list')
  , Lists = require('./lists')
  , TransitionGroup = React.addons.CSSTransitionGroup

module.exports = React.createClass({
	  displayName:'App'
	, getInitialState: function() {
		return { showLists:false }
	}
	, selectList: function(index) {
		this.setState({ showLists:false })
		this.props.current.set(index)
	}
	, showLists: function() {
		this.setState({ showLists:true })
	}
	, unShowLists: function() {
		this.setState({ showLists:false })
	}
	, onDeleteList: function() {
		var list = this.props.lists[this.props.current.val()]
		  , result = confirm('Are you sure you want to delete '+ list.title.val() +'?')

		if(result) {
			list.delete()
			if(this.props.lists.val().size <= 1) this.addList()
			
			this.props.current.set(0)
			this.setState({ showLists:true })
		}
	}
	, addList: function() {
		this.props.lists.unshift({
			  title: 'Untitled'
			, choices: []
			, timestamp: Date.now()
		})

		this.selectList(0)
		this.getDOMNode().querySelector('input.name').focus()
	}
	, render: function() {
		var list = this.props.lists[this.props.current.val()]
		  , lists

		if(this.state.showLists) {
			lists = <Lists lists={this.props.lists} onSelect={this.selectList} onBack={this.unShowLists} onAdd={this.addList} />
		} 

		return <div>
			<List list={list} onBack={this.showLists} onDelete={this.onDeleteList} />
			<TransitionGroup transitionName="lists">
				{lists}
			</TransitionGroup>
		</div>
	}
})