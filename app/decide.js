var React = require('react/addons')
  , PureRenderMixin = React.addons.PureRenderMixin
  , Decision = require('./decision')
  , Adsense = require('./adsense')

module.exports = React.createClass({
	  displayName:'Decide'
	, mixins: [PureRenderMixin]
	, getInitialState() {
		return { chosen:null }
	}
	, decide() {
		var choices = this.props.choices
		if(this.state.chosen)
			if(choices.val().size <= 1)
				return alert('This is your last choice.')

		var i = Math.floor(Math.random()*choices.val().size)
		  , choice = choices[i]

		if(this.state.chosen == choice)
			i = (++i)%choices.val().size
		
		this.setState({ chosen:choices[i] })
	}
	, undecide() {
		this.setState({ chosen:null })
	}
	, render() {
		var try_again_disabled = this.props.choices.val().size <= 1
	
		if(this.state.chosen) return <div className="decide page">
			<Adsense />
			<div className="container">
				<Decision chosen={this.state.chosen} choices={this.props.choices} />
			</div>
			<div className="action">
				<button className="again" disabled={try_again_disabled} onClick={this.decide}>Retry</button>
				<button className="done" onClick={this.undecide}>Back</button>
			</div>
		</div>

		return <div className="decide button" role="button" onClick={this.decide}>
			<span>&#10003;</span>
		</div>
	}
})