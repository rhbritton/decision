var React = require('react/addons')
  , PureRenderMixin = React.addons.PureRenderMixin

module.exports = React.createClass({
	  displayName:'Add'
	, mixins: [PureRenderMixin]
	, getInitialState: function() {
		return { addInput: '' }
	}
	, componentDidMount: function() {
		this.getDOMNode().querySelector('input').focus()
	}
	, addChoice: function(e) {
		var choice = {}

		choice.id = Date.now() + '-' + Math.random()
		choice.text = this.state.addInput
		choice.timestamp = new Date()
		
		if(choice.text && this.props.onAdd(choice))
			this.setState({ addInput:'' })

		this.getDOMNode().querySelector('input').focus()

		e.preventDefault()
	}
	, setInputValue: function(e) {
		if(e.target.value && e.target.value.length > 256)
			return alert('Your choice is too long.')

		this.setState({ addInput:e.target.value })
	}
	, render: function() {
		return (
			<div className="add section">
				<form className="choice" onSubmit={this.addChoice}>
					<input type="text" autocapitalize="on" onChange={this.setInputValue} value={this.state.addInput} />
					<button className="add" type="submit" onClick={this.addChoice}><span>+</span></button>
				</form>
			</div>
		)
	}
})