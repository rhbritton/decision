var React = require('react/addons')

module.exports = React.createClass({
	  displayName:'ListTitle'
	, onSelectList: function() {
		this.props.onSelectList(this.props.index)
	}
	, render: function() {
		return <button onClick={this.onSelectList}>
			<span className="name">{this.props.list.title.val()}</span>
			<span className="values">{this.props.list.choices.map(function(choice) {
				return choice.text.val()
			}).join(', ')}</span>
		</button>
	}
})

