var React = require('react/addons')
  , ListTitle = require('./list-title')

module.exports = React.createClass({
	  displayName:'Lists'
	, onSelectList: function(index) {
		this.props.onSelect(index)
	}
	, stopPropagation: function(e) {
		e.stopPropagation()
	}
	, render: function() {
		var self = this
		  , lists = this.props.lists.map(function(list, i) {
		  		return <ListTitle list={list} index={i} onSelectList={self.onSelectList} />
			})

		return <div className="lists" onClick={this.props.onBack}>
			<div className="sidebar" onClick={this.stopPropagation}>
				<div className="title"></div>
				<button className="add" onClick={this.props.onAdd}><span className="name">New List</span></button>
				{lists}
			</div>
		</div>
	}
})