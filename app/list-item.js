var React = require('react/addons')
  , PureRenderMixin = React.addons.PureRenderMixin

module.exports = React.createClass({
	  displayName:'ListItem'
	, mixins: [PureRenderMixin]
	, deleteChoice: function(e) {
		this.props.choice.delete()
	  }
	, updateChoice:function(e) {
		this.props.choice.text.set(e.target.value)
	  }
	, render: function() {
		return (
			<div className="choice">
				<input type="text" value={this.props.choice.text.val()} onChange={this.updateChoice} />
				<button className="delete" onClick={this.deleteChoice}><span>&times;</span></button>
			</div>
		)
	  }
})