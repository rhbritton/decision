var React = require('react/addons')
  , FastClick = require('fastclick')
  , App = require('./app')
  , Cereb = require('cereb')
  , data = JSON.parse(localStorage['data'] || null) || {
  		  lists: [{
  		  	  choices: []
  		  	, title: 'Untitled'
  		  }]
	  	, current: 0
  	}
  , ref = new Cereb(data, update)
  , appElement = document.getElementById('app')
  , app

function update(updatedRef) {
	localStorage['data'] = JSON.stringify(updatedRef)
	app.setProps({ lists:updatedRef.lists, current:updatedRef.current })
}

new FastClick(appElement)

app = React.render(<App lists={ref.lists} current={ref.current} />, appElement)